import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccordionModule, AlertModule } from 'ngx-bootstrap';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    AlertModule.forRoot(),
    AccordionModule.forRoot(),
  ],
  declarations: [],
  exports: [AlertModule, AccordionModule, FormsModule]
})
export class SharedModule { }
