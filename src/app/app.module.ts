import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AlertModule } from 'ngx-bootstrap';
import { IdeasModule } from './ideas/ideas.module';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [{
  path: '', pathMatch: 'full', redirectTo: '/ideas'
}];

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    IdeasModule,
    RouterModule.forRoot(routes),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
