import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IdeasComponent } from './ideas.component';
import { SharedModule } from '../shared/shared.module';
import { IdeaFormComponent } from './idea-form/idea-form.component';
import { ModalModule } from 'ngx-bootstrap';
import { IdeaModalService } from './idea-modal.service';
import { CounterModule } from 'angular2-component-library';
import { IdeasRoutingModule } from './ideas-routing.module';
import { LocalStorageModule } from 'angular-2-local-storage';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ModalModule.forRoot(),
    IdeasRoutingModule,
    CounterModule,
    LocalStorageModule.withConfig({
      prefix: 'my-app',
      storageType: 'localStorage'
    })
  ],
  declarations: [IdeasComponent, IdeaFormComponent],
  entryComponents: [IdeaFormComponent],
  providers: [IdeaModalService],
  exports: []
})
export class IdeasModule { }
