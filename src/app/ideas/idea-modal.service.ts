import { Injectable } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap';
import { IdeaFormComponent } from './idea-form/idea-form.component';
import { Idea } from './model/idea';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/do';

@Injectable()
export class IdeaModalService {

  constructor(private modalService: BsModalService) {
  }

  openIdeaFormModal(idea: Idea): Observable<Idea> {
    const modal = this.modalService.show(IdeaFormComponent);
    modal.content.idea = idea;

    return modal.content.ideaSubmitted
      .do(() => modal.hide());
  }
}
