import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IdeasComponent } from './ideas.component';
import { SharedModule } from '../shared/shared.module';
import { IdeaFormComponent } from './idea-form/idea-form.component';
import { ModalModule } from 'ngx-bootstrap';
import { IdeaModalService } from './idea-modal.service';
import { CounterModule } from 'angular2-component-library';
import { RouterModule } from '@angular/router';

const routes = [
  {path: 'ideas', component: IdeasComponent}
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class IdeasRoutingModule { }
