import { TestBed, inject } from '@angular/core/testing';

import { IdeaModalService } from './idea-modal.service';

describe('IdeaModalService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IdeaModalService]
    });
  });

  it('should be created', inject([IdeaModalService], (service: IdeaModalService) => {
    expect(service).toBeTruthy();
  }));
});
