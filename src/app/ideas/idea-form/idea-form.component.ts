import { Component, EventEmitter, Input, OnChanges, OnInit, Output, ViewChild } from '@angular/core';
import { Idea } from '../model/idea';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-idea-form',
  templateUrl: './idea-form.component.html',
  styleUrls: ['./idea-form.component.css']
})
export class IdeaFormComponent implements OnInit, OnChanges {

  @Input('idea')
  idea: Idea = {} as Idea;
  @Output() ideaSubmitted = new EventEmitter<Idea>();


  constructor() {
  }

  ngOnInit() {
  }

  @ViewChild('ideaForm') form;

  ngOnChanges(changes) {
    if (changes.idea) {
      this.form.resetForm(changes.idea);
    }
  }

  submitForm(form: NgForm, idea: Idea) {
    this.ideaSubmitted.emit(idea);
  }

}
