import { Component, OnInit } from '@angular/core';
import { Idea } from './model/idea';
import { IdeaModalService } from './idea-modal.service';
import { LocalStorageService } from 'angular-2-local-storage';

@Component({
  selector: 'app-ideas',
  templateUrl: './ideas.component.html',
  styleUrls: ['./ideas.component.css']
})
export class IdeasComponent implements OnInit {

  idea: Idea;

  ideas: Idea[] = [];

  constructor(private ideaModal: IdeaModalService,
              private localStorageService: LocalStorageService) {

    const ideas = this.localStorageService.get<Idea[]>('ideas');
    if (ideas) {
      this.ideas = ideas;
    }
  }

  ngOnInit() {
    this.resetIdea();
  }

  newIdea(idea: Idea) {
    this.ideas.push(idea);

    this.resetIdea();

    this.localStorageService.set('ideas', this.ideas);
  }

  editIdea(idea: Idea) {
    this.ideaModal.openIdeaFormModal(this.clone(idea))
      .subscribe(changedIdea => this.merge(idea, changedIdea));

    this.localStorageService.set('ideas', this.ideas);
  }

  private merge(idea: Idea, changedIdea) {
    return Object.assign(idea, changedIdea);
  }

  private clone(idea: Idea) {
    return Object.assign({}, idea);
  }

  private resetIdea() {
    this.idea = {
      name: '',
      description: '',
      timesHeardAbout: 0
    };
  }
}
