# Angular2DemoApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.2.3.


Vytvoření
```
ng new angular2-demo-app
```

```
ng serve
```

## Html

app.component.html

```
<div class="container">
  <div class="alert alert-success">Všechno funguje</div>
</div>
```

## Bootstrap

```
npm install bootstrap@3 --save
```

Přidat styl do: .angular-cli.json 

```
"styles": [
  "../node_modules/bootstrap/dist/css/bootstrap.css",
  "styles.css"
],
```

style.css 

```
body {
  padding-top: 30px;
}
```

## Angular - Bootstrap

Bootstrap - https://valor-software.com/ngx-bootstrap/#/

npm install ngx-bootstrap --save


V app.module.ts

```
AlertModule.forRoot()
```

app.component.html

```  
<alert type="success" [dismissible]="true">Všechno funguje</alert>
```

# Modul Ideas
 - vytvořit modul ideas
 - vytvoriž komponentu ideas
 - export ideas komponenty
 - použít v app.component.html
 
## Závislosti modulů 
  - přesunout alert do ideas.component.html    
  
  ```
  <alert type="success" [dismissible]="true">Všechno funguje</alert>
  ```
  
  - přidat do ideas 
  - případně vytvořit sharedModule a navázat
  - rovnou přidat formmodule
  
  
```
@NgModule({
  imports: [
    CommonModule,
    AlertModule.forRoot()
  ],
  declarations: [],
  exports: [AlertModule]
})
export class SharedModule { }
```

## Data a zobrazení

Model:
``` 
export interface Idea {
  name: string;
  description: string;
  timesHeardAbout?: number;
}
```

Data záznamů: 
```
  ideas: Idea[] = [
    {name: 'Automatizované otevírání kurníku', description: 'Aby slepičky neutíkaly', timesHeardAbout: 2},
    {name: 'hračka pro děti', description: 'Mám hromadu starých vypínačů'}
  ];
```

- přidat bootstrap accordeon
- module, for root, export
- cyklus přes idea accordeon panel

```
<accordion>
  <accordion-panel *ngFor="let idea of ideas" [heading]="idea.name">
    {{idea.description}}
  </accordion-panel>
</accordion>
```

- zobrazit alert, když už byl viděn

# Formulář pro přidávání

```
<div class="well">
  <form>
    <div class="form-group">
      <label for="name">Název</label>
      <input type="text" class="form-control" name="name" id="name" placeholder="Název" />
    </div>

    <div class="form-group">
      <label for="description">Popis</label>
      <textarea class="form-control" id="description" placeholder="Popis" rows="3" name="description"></textarea>
    </div>

    <button type="submit" class="btn btn-default btn-success">Uložit</button>
  </form>
</div>
```

## Ukládání 
- Oživit ngModel a ukládání do ideas v ideas.component.ts

```
  newIdea(idea: Idea) {
    this.ideas.push(idea);

    this.resetIdea();
  }
  
  private resetIdea() {
    this.idea = {
      name : '',
      description : '',
      timesHeardAbout : 0
    };
  } 
  
```

## Zapouzdření komponenty

```
ng g component ideas/idea-form

```

### Použití

```
<app-idea-form [idea]="idea" (ideaSubmitted)="submitForm($event)"></app-idea-form>
```
 
## Použití v modálním okně

- bootstrap modal
- provide v ideas.module

### Služba pro otevření okna
```
ng g service idea/ideaModal

``` 

- vytvořit metodu pro otevření 
- zobrazit IdeaFormComponent
- nastavit content.idea
- pověsit se na ideaChange a volat hide()

```
  openIdeaFormModal(idea: Idea): Observable<Idea> {
    const modal = this.modalService.show(IdeaFormComponent);
    modal.content.idea = idea;

    return modal.content.ideaChange
      .do(() => modal.hide());
  }
```

- volání metody editIdea v ideas.component.html 
- kopie objektu pomocí Object.assign směrem tam i ven

```
  editIdea(idea: Idea) {
    this.ideaModal.openIdeaFormModal(this.clone(idea))
      .subscribe(changedIdea => this.merge(idea, changedIdea));

  }
  
   private merge(idea: Idea, changedIdea) {
     return Object.assign(idea, changedIdea);
   }
  
   private clone(idea: Idea) {
     return Object.assign({}, idea);
   }

``` 
 
# Vlastní komponenta

 - přejít do vytváření komponenty

```
npm install --save https://bitbucket.org/angular_cz/angular2-component-library-npm
```

# Validace

```
<alert type="danger"
       *ngIf="name.hasError('required') && (name.touched || ideaForm.submitted)">
  Musíte zadat jméno
</alert>
```

```
  private resetIdea() {
    this.idea = {
      name : '',
      description : '',
      timesHeardAbout : 0
    };
  }
```


Při odesílání nutno pozor na odeslání idea
```
  submitForm(form: NgForm) {
    this.ideaSubmitted.emit(idea);
    form.resetForm(idea);
  }
```

- kopie při založení
- správně resetovat až při příchozím požadavku

```
  @ViewChild('ideaForm') form;

  ngOnChanges(changes) {
    if (changes.idea) {
      this.form.resetForm(changes.idea);
    }
  }
```

# Routing 

- do samostatného modulu
- zrušit export IdeasComponenty
- path, pathMatch = 'full'

```
const routes: Routes = [{
  path: '', pathMatch: 'full', redirectTo: '/ideas'
}];
```

# Local store

https://www.npmjs.com/package/angular-2-local-storage

```
npm install --save angular-2-local-storage
```

## Ukládání

```
this.localStorageService.set('ideas', this.ideas);
```

## Načtení
```
const ideas = this.localStorageService.get<Idea[]>('ideas');
if (ideas) {
  this.ideas = ideas;
}
```

## Nasazení

- Vytvořit aplikaci
- podívat se na deploy
- firebase login
- firebase init
- firebase build 
- firebase deploy
