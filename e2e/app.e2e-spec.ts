import { Angular2DemoAppPage } from './app.po';

describe('angular2-demo-app App', () => {
  let page: Angular2DemoAppPage;

  beforeEach(() => {
    page = new Angular2DemoAppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
